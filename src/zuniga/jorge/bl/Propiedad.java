/*
 * 
 */
package zuniga.jorge.bl;

/**
 *
 * @author jz600
 * @version 1
 * @since 10-02-2021
 * 
 * Esta clase gestiona las propiedades del sistema
 */
public class Propiedad {
    private int codigo, cantCuartos;
    private String nombre, provincia;
    private double alquiler;

    /**
    * Constructor por defecto, no recibe parámetros.
    */
    public Propiedad() {
    }

    /**
    * Constructor con parámetros.
    * @param codigo número entero, es el código asignado a la propiedad.
    * @param cantCuartos número entero, representa la cantidad de cuartos disponibles en cada propiedad.
    * @param nombre String, representa el nombre asignado a cada propiedad.
    * @param provincia String, representa el nombre de la provincia en que la propiedad se ubica.
    * @param alquiler Double, representa el monto cobrado mensualmente por alquiler.
    */
    public Propiedad(int codigo, int cantCuartos, String nombre, String provincia, double alquiler) {
        this.codigo = codigo;
        this.cantCuartos = cantCuartos;
        this.nombre = nombre;
        this.provincia = provincia;
        this.alquiler = alquiler;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCantCuartos() {
        return cantCuartos;
    }

    public void setCantCuartos(int cantCuartos) {
        this.cantCuartos = cantCuartos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public double getAlquiler() {
        return alquiler;
    }

    public void setAlquiler(double alquiler) {
        this.alquiler = alquiler;
    }
    
    /**
     * Método toString para poder obtener toda la información del objeto como un String.
     * @return Un String con toda la información del objeto. 
     */
    @Override
    public String toString() {
        return "Propiedad{" + "Código=" + codigo + ", Cantidad de cuartos =" + cantCuartos + ", Nombre=" + nombre +
                ", Provincia=" + provincia + ", Monto de alquiler=" + alquiler + '}';
    }
    
}
