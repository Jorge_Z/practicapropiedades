/*
 * 
 */
package zuniga.jorge.ui;
import zuniga.jorge.bl.Propiedad;
import java.io.*;
/**
 *
 * @author jz600
 * @version 2.0
 * @since 10-02-2021
 */
public class PracticaPropiedades {

    public static BufferedReader in = new BufferedReader (new InputStreamReader(System.in));
    public static PrintStream out = System.out;
    public static Propiedad[]regPropiedades = new Propiedad[10];
    public static void main(String[] args) throws IOException {
        menu();
    }
    
    public static void menu() throws IOException{
        int opcion;
        do {
            out.println("--------------- Registro de Propiedades ---------------");
            out.println("-------------------------------------------------------");
            out.println("Seleccione una de las siguientes opciones:");
            out.println("1. Registrar una nueva propiedad.");
            out.println("2. Mostrar propiedades.");
            out.println("3. Buscar propiedad por nombre.");
            out.println("4. Mostrar total de alquiler.");
            out.println("5. Salir");
            out.println("Ingrese su opción:");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while (opcion != 5);
    }
    
    public static void procesarOpcion(int popcion) throws IOException{
        switch(popcion){
            case 1:
                registrarPropiedad();
                break;
            case 2:
                mostrarPropiedades();
                break;
            case 3:
                buscarPropiedad();
                break;
            case 4:
                sumarAlquiler();
                break;
            case 5:
                out.println("Gracias por usar el sistema.");
                break;
            default:
                out.println("Opción incorrecta, por favor verifique su selección.");
                out.println();
        }
    }
    
    public static void registrarPropiedad() throws IOException {
        int codigo, cantCuartos, i;
        String nombre, provincia;
        double alquiler;
        out.println("Ingrese el código de la propiedad:");
        codigo = Integer.parseInt(in.readLine());
        out.println("Ingrese el nombre:");
        nombre = in.readLine();
        out.println("Ingrese la cantidad de habitaciones:");
        cantCuartos = Integer.parseInt(in.readLine());
        out.println("Escriba el nombre de la provincia en que está la propiedad:");
        provincia = in.readLine();
        out.println("Escriba el monto cobrado por alquiler:");
        alquiler = Double.parseDouble(in.readLine());
        Propiedad nuevoRegistro = new Propiedad(codigo, cantCuartos, nombre, provincia, alquiler);
        for(i = 0; i<regPropiedades.length; i++){
            if (regPropiedades[i] == null){
                regPropiedades[i] = nuevoRegistro;
                i = regPropiedades.length;
            } else if (regPropiedades[i].getCodigo() == nuevoRegistro.getCodigo() ) {
                out.println("Ese código ya fue usado.");
                out.println("Por favor intente de nuevo.");
                out.println("*************************");
                i = regPropiedades.length;
            }
        }
    }
    
    public static void mostrarPropiedades(){
        int i;
        out.println("Propiedades registradas.");
        out.println();
        for (i = 0; i<regPropiedades.length; i++){
            if(regPropiedades[i] != null){
                out.println(regPropiedades[i].toString());
            }
        }
        out.println("*************************");
    }
    
    public static void buscarPropiedad() throws IOException{
        int i;
        String nombre;
        out.println("Ingrese el nombre de la propiedad a buscar:");
        nombre = in.readLine();
        for (i = 0; i<regPropiedades.length; i++){
            if (regPropiedades[i] != null) {
                if (regPropiedades[i].getNombre().equals(nombre)) {
                    out.println(regPropiedades[i].toString());
                    i = regPropiedades.length;
                }
            } else {
                out.println("Ninguna propiedad corresponde con el nombre ingresado.");
                out.println("*************************");
                i = regPropiedades.length;
            }
        }
    }
    
    public static void sumarAlquiler(){
        int i;
        double tempAlquiler, totalAlquiler = 0;
        if(regPropiedades[0] != null){
            for(i = 0; i<regPropiedades.length; i++){
            if (regPropiedades[i] != null){
                tempAlquiler = regPropiedades[i].getAlquiler();
                totalAlquiler += tempAlquiler;
                }
            }
            out.println("El total mensual por alquileres es de "+(String.format ("%.2f", totalAlquiler)));
            out.println("*************************");
        } else {
            out.println("Aún no hay montos de alquiler registrados.");
            out.println("*************************");
        }
    }
}
